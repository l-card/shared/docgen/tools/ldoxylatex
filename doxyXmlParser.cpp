#include "doxyXmlParser.h"
#include <QFile>
#include <QDir>

static void get_func_param_list(QDomElement elem, QList<doxyElemFuncPar> &par, QDomElement &ret)
{
    /* список параметров удаляем из подробного описания и сохраняем в отдельный
       список, так как его обрабатываем отдельно */
    QDomNodeList nodeList = elem.elementsByTagName("parameteritem");
    for (int i=nodeList.length()-1; i>=0; i--)
    {
        par.insert(0, doxyElemFuncPar(nodeList.item(i).toElement()));
        /* удаляем разобранные узлы */
        nodeList.item(i).parentNode().removeChild(nodeList.item(i));
    }

    /* находим возвращаемый результат */
    nodeList = elem.elementsByTagName("simplesect");
    for (int i=nodeList.length()-1; i>=0; i--)
    {
        /* удаляем разобранные узлы */
        if (nodeList.item(i).toElement().attribute("kind")=="return")
        {
            ret  = nodeList.item(i).toElement();
            nodeList.item(i).parentNode().removeChild(nodeList.item(i));
        }
    }
}


void doxyElemDescr::parseFromElemnt(QDomElement elem)
{
    m_id = elem.attribute("id");

    for (QDomElement valElem = elem.firstChildElement(); !valElem.isNull();
         valElem=valElem.nextSiblingElement())
    {
        parseChild(valElem);
    }

    if (m_briefDescr.isEmpty())
    {
        m_briefDescr = m_descr.text();
    }
}

void doxyElemDescr::parseChild(QDomElement elem)
{
    if (elem.tagName()=="name")
    {
        m_name = elem.text();
    }
    else if (elem.tagName()=="briefdescription")
    {
        m_briefDescr = elem.text();
    }
    else if (elem.tagName()=="detaileddescription")
    {
        m_descr = elem;
    }
}

void doxyElemEnumVal::parseChild(QDomElement elem)
{
    if (elem.tagName() == "initializer")
    {
        m_initializer = elem.text().trimmed();
        /* удаляем начальное = в инициализаторе */
        if (m_initializer.at(0)=='=')
            m_initializer.remove(0,1);
        m_initializer = m_initializer.trimmed();
    }
    else
    {
        doxyElemDescr::parseChild(elem);
    }
}

void doxyElemEnum::parseChild(QDomElement elem)
{
    if (elem.tagName()=="enumvalue")
    {
        m_vals << doxyElemEnumVal(elem);
    }
    else
    {
        doxyElemDescr::parseChild(elem);
    }
}


void doxyElemFuncPar::parseChild(QDomElement elem)
{
    if(elem.tagName()=="parameternamelist")
    {
        QDomElement name = elem.elementsByTagName("parametername").at(0).toElement();
        if (!name.isNull())
        {
            QString dir = name.attribute("direction");
            if (dir=="in")
                m_dir = FUNC_PARAM_DIR_IN;
            else if (dir=="out")
                m_dir = FUNC_PARAM_DIR_OUT;
            else if (dir=="in,out")
                m_dir = FUNC_PARAM_DIR_INOUT;

            m_name = name.text();
        }
    }
    else if (elem.tagName()=="parameterdescription")
    {
        m_descr = elem;
    }
}

void doxyElemFunc::parseFromElemnt(QDomElement elem)
{
    get_func_param_list(elem, m_params, m_retElem);

    doxyElemDescr::parseFromElemnt(elem);
}

void doxyElemFunc::parseChild(QDomElement elem) {
    if (elem.tagName()=="definition") {
        m_def = elem.text();
    } else if (elem.tagName()=="argsstring") {
        /* так как при typedef argstring дублируется в "definition" и начинается с
         * ')', то не включаем в этом случае argstring */
        m_argstr = elem.text().trimmed();
        if (!m_argstr.isEmpty() && m_argstr.at(0)==')')
            m_argstr.clear();
    } else {
        doxyElemDescr::parseChild(elem);
    }

}

void doxyElemType::parseChild(QDomElement elem)
{
    if (elem.tagName()=="compoundname")
    {
        m_name = elem.text();
    }
    else if ((elem.tagName()=="sectiondef") && (elem.attribute("kind")=="public-attrib"))
    {
        /* ищем все открытые поля структуры */
        for (QDomElement memberElem = elem.firstChildElement(); !memberElem.isNull();
             memberElem=memberElem.nextSiblingElement())
        {
            if ((memberElem.tagName()=="memberdef") && (memberElem.attribute("kind")=="variable"))
            {
                m_fields << doxyElemField(memberElem);
            }
        }
    }
    else
    {
        doxyElemDescr::parseChild(elem);
    }
}



void doxyElemField::parseChild(QDomElement elem)
{
    if (elem.tagName()=="type")
    {
        m_type = elem;
    }
    else if (elem.tagName()=="argsstring")
    {
        m_typeModifier = elem;
    }
    else
    {
        doxyElemDescr::parseChild(elem);
    }
}


void doxyCompoundDescr::parseFromElemnt(QDomElement elem)
{
    m_kind = elem.attribute("kind");
    doxyElemDescr::parseFromElemnt(elem);

    QDomNodeList members = elem.elementsByTagName("memberdef");
    for (int i=0; i < members.length(); i++)
    {
        if (members.at(i).isElement())
        {            
            QDomElement member = members.at(i).toElement();

            QString kind = member.attribute("kind");
            QString id = member.attribute("id");

            if (!(member.attribute("static").trimmed()=="yes"))
            {
                if (kind=="function")
                {
                    m_funcList << doxyElemFunc(member);
                }
                else if (kind == "enum")
                {
                    m_enumList << doxyElemEnum(member);
                }
                else if (kind == "define")
                {
                    m_definesList << doxyElemEnumVal(member);
                }
                else if (kind == "typedef")
                {
                    m_structList << new doxyElemType(member);
                }
                else if (kind == "variable")
                {
                    int fnd = 0;

                    foreach (doxyElemField* field, m_variablesList)
                    {
                        if (field->id()==id)
                            fnd = 1;
                    }


                    if (!fnd)
                        m_variablesList << new doxyElemField(member);
                }
            }
        }
    }
}

void doxyCompoundDescr::parseChild(QDomElement elem)
{
    if (elem.tagName()=="compoundname")
    {
        m_name = elem.text();
    }
    else if (elem.tagName()=="title")
    {
        m_title = elem.text();
    }
    else if (elem.tagName()=="innerfile")
    {
        QString refid = elem.attribute("refid");
        QDir dir(m_filename);
        dir.cdUp();

        QList<doxyElemDescr*> clist;
        if ((parseDoxyFile(dir.absoluteFilePath(refid+".xml"), clist)==0) && (clist.size()))
        {
            for (int i=0; i < clist.size(); i++)
            {
                doxyCompoundDescr* cdescr = dynamic_cast<doxyCompoundDescr*>(clist.at(i));
                if (cdescr && (cdescr->kind()=="file"))
                {
                    m_fileList << cdescr;
                }
            }
        }
    }
    else if (elem.tagName()=="innerclass")
    {
        QString refid = elem.attribute("refid");
        QDir dir(m_filename);
        dir.cdUp();

        QList<doxyElemDescr*> clist;
        if ((parseDoxyFile(dir.absoluteFilePath(refid+".xml"), clist)==0) && (clist.size()))
        {
            for (int i=0; i < clist.size(); i++)
            {
                doxyElemType* cdescr = dynamic_cast<doxyElemType*>(clist.at(i));
                if (cdescr)
                {
                    m_structList << cdescr;
                }
            }
        }
    }
    else
    {
        doxyElemDescr::parseChild(elem);
    }
}



int parseDoxyFile(QString filename, QList<doxyElemDescr*>& list)
{
    QDomDocument doc;
    QFile file(filename);
    int err = 0;

    if (!file.open(QIODevice::ReadOnly))
         err = -4;
    else
    {
        if (!doc.setContent(&file))
        {
             err = -5;
        }
        file.close();
    }

    if (!err)
    {
        QDomElement docElem = doc.documentElement();
        if (docElem.tagName()!="doxygen")
           err = -7;
        else
        {
             QDomNode n = docElem.firstChild();
             while(!n.isNull())
             {
                QDomElement e = n.toElement(); // try to convert the node to an element.
                if(!e.isNull() && e.tagName()=="compounddef" && !(e.attribute("static").trimmed()=="yes"))
                {
                    if (e.attribute("kind")=="struct")
                    {
                        list << new doxyElemType(e);
                    }
                    else
                    {
                        list << new doxyCompoundDescr(e, filename);
                    }
                }
                n = n.nextSibling();
             }
        }
    }
    return err;
}
