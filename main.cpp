#include <QtXml>
#include <QString>

#include "doxyXmlParser.h"


static const double param_field_name_len_max = 0.25;
static const double param_field_name_len_min = 0.12;
static const double param_field_val_len_max = 0.32;
static const double param_field_val_len_min = 0.18;

static const double param_const_name_len_max = 0.4;
static const double param_const_name_len_min = 0.15;
static const double param_const_val_len_max = 0.23;
static const double param_const_val_len_min = 0.12;

static const int    param_nonbreak_const_vals = 2;

static QString sect_lvls[] = {"\\chapter","\\section", "\\subsection", "\\subsubsection", "\\paragraph\\newline", "\\subparagraph\\newline"};

static QStringList sect_id_list;

typedef struct {
    QString before;
    QString after;
} t_replace;

static t_replace f_replaces[] = {
    {"][", "] ["}
};

static t_replace f_id_replaces[] = {{"_1", "_"}};


static void f_correct_quote(QString &text, QString quot) {
    int ind, ind2;
    do {
        /* замена кавычек на принятый в tex вариант с `` и ''*/
        /** @todo Рассмотреть возможность варианта, когда открытие
         * и закрытие выполняется в разных вызовах f_save_plain_text() */
        ind = text.indexOf(quot);
        if (ind >= 0) {
            ind2 = text.indexOf(quot, ind+quot.length());
            if (ind2 >= 0) {
                text.replace(ind, quot.length(), "``");
                ind = text.indexOf(quot);
                text.replace(ind, quot.length(), "''");
            }
        }
    } while ((ind >= 0) && (ind2 >= 0));
}

static inline double line_perc_from_charcnt(int char_cnt) {
    return (double)char_cnt * 1.25/100;
}

static inline QString f_table_section_start(void) {
     return "\\normalLTfalse\n";
}

static inline QString f_table_section_end(void) {
     return "\\normalLTtrue\n";
}

static inline QString f_allow_underscore_linebreak(QString text) {
    return text.replace("\\_", "\\_\\allowbreak ");
}

static inline QString f_allow_underscore_linebreak_perc(QString text, double perc) {
    return line_perc_from_charcnt(text.length()) < perc ? text : f_allow_underscore_linebreak(text);
}

static QString f_save_plain_text(QString text, bool tex_src = false) {
    if (!tex_src) {
        for (uint i=0; i < sizeof(f_replaces)/sizeof(f_replaces[0]); i++) {
            text.replace(f_replaces[i].before, f_replaces[i].after);
        }

        f_correct_quote(text, "\"");

        text.replace("_","\\_");
        text.replace("#","\\#");
        text.replace("&","\\&");
        text.replace("<<", "$<<$");
        text.replace(">>", "$>>$");
    }
    return text;
}

static QString f_save_text(QDomElement elem, bool plain = false, bool tex_src = false);

static QString f_save_item_list(QDomElement elem) {
    QString ret;
    ret.append("\\begin{itemize}[nolistsep]");
    for (QDomNode node = elem.firstChild(); !node.isNull(); node = node.nextSibling()) {
        if (node.isElement()) {
            QDomElement itemElem = node.toElement();
            if (itemElem.tagName() == "listitem") {
                ret.append("\\item ");
                ret.append(f_save_text(itemElem, false, false));
            }
        }
    }
    ret.append("\\end{itemize}");
    return ret;
}

/* сохраняем текст, описанный через Xml как latex-текст.
   - вставляет пустые строки между параграфами
   - обрабатывает ссылки (узел типа ref)
   - note
   */
static QString f_save_text(QDomElement elem, bool plain, bool tex_src) {
    int prev_para = 0;
    QString str;


    for (QDomNode node = elem.firstChild(); !node.isNull(); node = node.nextSibling()) {
        //текст сохраняем как есть
        if (node.isText()) {
            str.append(f_save_plain_text(node.toText().data(), tex_src));
        } else if (node.isElement()) {
            QDomElement elem = node.toElement();
            QString tag = elem.tagName();
            //ссылка на другое место
            if (tag =="ref") {
                QString kind = elem.attribute("kindref");
                QString id = elem.attribute("refid");

                for (uint i=0; i < sizeof(f_id_replaces)/sizeof(f_id_replaces[0]); i++) {
                    id.replace(f_id_replaces[i].before, f_id_replaces[i].after);
                }

                int close = 0;
                if ((kind=="member") && !plain) {
                    str.append("\\texttt{");
                    close = 1;
                }

                if (!plain) {
                    bool sec_ref = false;
                    Q_FOREACH(QString sec_id, sect_id_list) {
                        if (sec_id.trimmed()==id.trimmed()) {
                            sec_ref = true;
                            break;
                        }
                    }

                    /* ссылку на секции делаем через hyperref, а остальные через
                     * hyperlink */
                    if (sec_ref) {
                        str.append("\\hyperref[").append(id).append("]{");
                    } else {
                        str.append("\\hyperlink{").append(id).append("}{");
                    }
                }

                QString valText = f_save_plain_text(elem.text());
                valText = f_allow_underscore_linebreak_perc(valText, 0.3);
                str.append(valText);

                if (!plain)
                    str.append("}");

                if (close)
                    str.append("}");
            } else if (tag == "ulink") {
                QString url = elem.attribute("url");
                str.append(QString("\\href{%1}{%2}").arg(url).arg(f_save_plain_text(elem.text())));
            } else if (tag == "simplesect") {
                 if (elem.attribute("kind")=="note") {
                     /** @todo добавить обрамление для Note */
                     str.append(f_save_text(elem, plain));
                 }
            } else if (tag=="para") { //параграф с текстом
                //если до этого уже был параграф, то ставим разделитель
                if (prev_para && !elem.text().trimmed().isEmpty() && !plain)
                    str.append("\n\n");
                prev_para = 1;
                str.append(f_save_text(elem, plain));

            } else if (tag=="verbatim") {
                /** @todo Понять, при каких условиях появляется и как обрабатывать */
                str.append("\\verb+").append(f_save_text(elem, plain)).append("+");
            } else if (tag=="formula") {
                /* формулу оставляем как есть, так как она уже в tex-формате */
                str.append(f_save_text(elem, plain, true));
            } else if (tag=="mdash") {
                str.append("---");
            } else if (tag=="itemizedlist") {
                str.append(f_save_item_list(elem));
            }
        }
    }
    return str;
}



static QString f_save_label(QString id) {
    for (uint i=0; i < sizeof(f_id_replaces)/sizeof(f_id_replaces[0]); i++) {
        id.replace(f_id_replaces[i].before, f_id_replaces[i].after);
    }
    return "\\label{" + id  + "}";
}


static QString f_save_hypertarget_start(QString id) {
    for (uint i=0; i < sizeof(f_id_replaces)/sizeof(f_id_replaces[0]); i++) {
        id.replace(f_id_replaces[i].before, f_id_replaces[i].after);
    }
    return "\\hypertarget{" + id  + "}{";
}

static QString f_save_hypertarget_end(void) {
    return "}";
}

static QString f_save_hypertarget(QString id) {
    return f_save_hypertarget_start(id) + f_save_hypertarget_end();
}

/* Обычная ссылка привязывается к низу текста, что приводит к неверному переходу
   по крайней мере на текст в таблицах (т.к. строка, на которую выполняется переход,
   оказывается выше экрана). Для избежания этого поднимаем якорь привязки */
static QString f_save_raised_hypertarget(QString id) {
    for (uint i=0; i < sizeof(f_id_replaces)/sizeof(f_id_replaces[0]); i++) {
        id.replace(f_id_replaces[i].before, f_id_replaces[i].after);
    }
    return "\\raisebox{\\ht\\strutbox}{\\hypertarget{" + id  + "}{}}";
}

static QString f_save_ref_text(QString text, QString id) {
    return  f_save_plain_text(text) + f_save_label(id) + f_save_hypertarget(id);
}

static QString f_save_section(QString text, QString id, int lvl) {
    for (uint i=0; i < sizeof(f_id_replaces)/sizeof(f_id_replaces[0]); i++) {
        id.replace(f_id_replaces[i].before, f_id_replaces[i].after);
    }

    if (!sect_id_list.contains(id))
        sect_id_list.append(id);
    return f_save_ref_text(sect_lvls[lvl] + "{" + text.trimmed() + "}", id) + "\n";
}



static int f_save_func_param(QTextStream &stream, doxyElemFunc &func_descr) {
    //Список параметров (если есть)
    if (func_descr.paramsCnt()) {
        stream << QString("\\textbf{Параметры:} ");
        stream << "\\begin{description}[nolistsep]" << endl;
        for (int i=0; i < func_descr.paramsCnt(); i++) {
            stream << "\\item[" << f_save_plain_text(
                          func_descr.param(i).name()) << "] --- ";
            stream << f_save_text(func_descr.param(i).descrElem());
            stream << endl;
        }
        stream << "\\end{description}" << endl;
        /* после переноса строки в конце description добавляется еще перенос
           в конце столбца. Чтобы убрать лишную пустую строку добавляем -2ex.
           Если нет возвращаемого значения, то это последняя строка и
           поднятие линии приведет к артефактам */
        if (!func_descr.returnElem().isNull()) {
            stream << "\\\\*[-2ex]\\nobreakhline" << endl;
        } else {
            stream << "\\\\\\nobreakhline" << endl;
        }
    }

    //Возвращаемое значение (если есть)
    if (!func_descr.returnElem().isNull()) {
        stream << QString("\\textbf{Возвращаемое значение:}") << endl << endl;
        stream << f_save_text(func_descr.returnElem());
        stream << "\\\\\\hline" << endl;
    }

    return 0;
}


static int f_save_func_descr(QTextStream &stream, doxyElemFunc func_descr, int lvl) {
    stream << f_table_section_start();
    stream << f_save_section(func_descr.briefDescr(), func_descr.id(), lvl);

    stream << "\\begin{longtable}{|p{0.95\\linewidth}|} \\nobreakhline" << endl;

    stream << QString("\\multicolumn{1}{|>{\\raggedright\\arraybackslash}p{0.95\\linewidth}|}{\\textbf{Формат: \\texttt{") <<
    f_save_plain_text(func_descr.definition()) << "}}}\\\\\\nobreakhline" << endl;

    // Описание
    stream << QString("\\textbf{Описание:} ");
    /* разрешение отступов абзаца внутри описания функции */
    stream << "\\setlength\\parindent{1em}" << endl << endl;
    stream << f_save_text(func_descr.descrElem());
    stream << "\\\\\\nobreakhline" << endl;

    f_save_func_param(stream, func_descr);

    stream << "\\end{longtable}" << endl;
    stream << f_table_section_end();
    return 0;
}


static int f_save_func_type(QTextStream &stream, doxyElemFunc func_descr, int lvl) {
    stream << endl << f_save_section(func_descr.briefDescr(), func_descr.id(), lvl);

    stream << "\\begin{tabular}{|p{0.95\\linewidth}|} \\hline" << endl;

    stream << QString("\\textbf{Тип:} \\texttt{");
    stream << f_save_plain_text(func_descr.name());
    stream << "}\\\\\\hline" << endl;

    stream << QString("\\textbf{Определение: \\texttt{") <<
    f_save_plain_text(func_descr.definition()) << "}}\\\\\\hline" << endl;

    // Описание
    stream << QString("\\textbf{Описание:} ");
    stream << f_save_text(func_descr.descrElem());
    stream << "\\\\\\hline" << endl;

    f_save_func_param(stream, func_descr);


    stream << "\\end{tabular}" << endl;
    return 0;
}

static void f_save_enum_val(QTextStream& stream, doxyElemEnumVal val_descr, bool en_line_break) {
    stream << "\\texttt{\\small " << f_allow_underscore_linebreak_perc(f_save_plain_text(val_descr.name()), param_const_name_len_max) << "} &";
    stream << "\\texttt{\\small " << f_allow_underscore_linebreak_perc(f_save_plain_text(val_descr.initializer()), param_const_val_len_max) << "}";
    stream << "&";
    stream << f_save_raised_hypertarget(val_descr.id());
    stream << f_save_text(val_descr.descrElem());
    stream << f_save_label(val_descr.id()) << endl;
    if (en_line_break) {
        stream << "\\\\\\hline" << endl;
    } else {
        stream << "\\\\*\\nobreakhline" << endl;
    }
}

/* Запись в поток заголовка таблицы для сохранения значений констант и перечислений.
 * В tableType сохраняется название выбранной таблицы, чтобы ее корректно завершить */
static void f_save_const_table_hdr(QTextStream &stream, const QList<doxyElemEnumVal> vals, double *ptotal_len = 0) {
    double total_len = 0.9;

    int max_char_name = 0;
    int max_char_val = 0;

    for (int i=0; i < vals.length(); i++) {
         int len = vals.at(i).name().length();
         if (len > max_char_name)
             max_char_name = len;
         len = f_save_plain_text(vals.at(i).initializer()).length();
         if (len > max_char_val)
             max_char_val = len;
    }

    double name_width = line_perc_from_charcnt(max_char_name);
    if (name_width > param_const_name_len_max)
        name_width = param_const_name_len_max;
    if (name_width < param_const_name_len_min)
        name_width = param_const_name_len_min;
    double val_width = line_perc_from_charcnt(max_char_val);
    if (val_width > param_const_val_len_max)
        val_width = param_const_val_len_max;
    if (val_width < param_const_val_len_min)
        val_width = param_const_val_len_min;


    stream << "\\begin{longtable}{|m{" << name_width <<
              "\\linewidth}|m{" << val_width << "\\linewidth}|m{" <<
              total_len - name_width - val_width << "\\linewidth}|}\\nobreakhline" << endl;

    if (ptotal_len)
        *ptotal_len = total_len;
}

/* сохранение значения массива констант в таблице, ранее созданной с записанной
 * с помощью f_save_const_table_hdr() */
static void f_save_const_vals(QTextStream &stream, const QList<doxyElemEnumVal> vals) {
    stream << QString("\\textbf{Константа} & \\textbf{Значение} & \\textbf{Описание}\\\\*\\nobreakhline");

    for (int i=0; i < vals.length(); i++) {
        f_save_enum_val(stream, vals.at(i), i >= param_nonbreak_const_vals);
    }
    stream << "\\end{longtable}" << endl;
}


/* Сохранение типа перечисления */
static int f_save_enum_descr(QTextStream &stream, doxyElemEnum &enum_descr, int lvl) {
    double total_len;

    stream << f_table_section_start();
    /* объявляем секцию для описания перечисления */
    stream << endl << f_save_section(enum_descr.briefDescr(), enum_descr.id(), lvl);

    /* сохраняем заголовок аналогично константам*/
    f_save_const_table_hdr(stream, enum_descr.valList(), &total_len);

    /* сохраняем название типа */
    stream << "\\multicolumn{3}{|p{" << total_len << QString("\\linewidth}|}{\\textbf{Тип:} \\texttt{");
    stream << f_save_plain_text(enum_descr.name());
    stream << "}}\\\\*\\nobreakhline" << endl;

    /* сохраняем описание типа перечисления */
    stream << "\\multicolumn{3}{|p{" << total_len << QString("\\linewidth}|}{\\textbf{Описание:}") << endl;
    stream << f_save_text(enum_descr.descrElem());
    stream << "}\\\\*\\nobreakhline" << endl;

    f_save_const_vals(stream, enum_descr.valList());

    stream << f_table_section_end();
    return 0;
}

/* сохранение массива констант в отдельной таблице */
static int f_save_defines(QTextStream& stream, QList<doxyElemEnumVal> defs, int lvl) {
    f_save_const_table_hdr(stream, defs);
    f_save_const_vals(stream, defs);
    return 0;
}


static int f_save_field_descr(QTextStream &stream, const doxyElemField *field_descr, bool en_line_break) {
    stream << "\\texttt{" << f_allow_underscore_linebreak_perc(f_save_plain_text(field_descr->name()), param_field_name_len_max) << "} &";
    stream << "\\texttt{";
    if (field_descr->typeModifier().text().length())
        stream << "\\small{";
    stream << f_allow_underscore_linebreak_perc(f_save_text(field_descr->type()), param_field_val_len_max)
           << " " << f_allow_underscore_linebreak_perc(f_save_text(field_descr->typeModifier()), param_field_val_len_max);


    if (field_descr->typeModifier().text().length())
       stream << "}";
    stream << "} &";

    if (!field_descr->id().isEmpty()) {
        stream << f_save_raised_hypertarget(field_descr->id());
    }

    stream << f_save_text(field_descr->descrElem());

    if (!field_descr->id().isEmpty()) {
        stream << f_save_label(field_descr->id()) << endl;
    }
    if (en_line_break) {
        stream << "\\\\\\hline" << endl;
    } else {
        stream << "\\\\*\\nobreakhline" << endl;
    }
    return 0;
}

static int f_save_struct(QTextStream &stream, doxyElemType *struct_descr, int lvl) {
    int max_char_name = 0;
    int max_char_type = 0;

    /* запрет переноса страницы */
    stream << f_table_section_start();
    stream << f_save_section(struct_descr->briefDescr(), struct_descr->id(), lvl);

    for (int i=0; i <struct_descr->fieldsCount(); i++) {
         int len = struct_descr->field(i)->name().length();
         if (len > max_char_name)
             max_char_name = len;
         len = f_save_text(struct_descr->field(i)->type(), true).length();
         if (len > max_char_type)
             max_char_type = len;
         len = f_save_text(struct_descr->field(i)->typeModifier(), true).length();
         if (len > max_char_type)
             max_char_type = len;
    }

    double name_width = line_perc_from_charcnt(max_char_name);
    if (name_width > param_field_name_len_max)
        name_width = param_field_name_len_max;
    if (name_width < param_field_name_len_min)
        name_width = param_field_name_len_min;
    double val_width = line_perc_from_charcnt(max_char_type);
    if (val_width > param_field_val_len_max)
        val_width = param_field_val_len_max;
    if (val_width < param_field_val_len_min)
        val_width = param_field_val_len_min;


    stream << "\\begin{longtable}{|m{" << name_width << "\\linewidth}|m{" <<
              val_width << "\\linewidth}|m{" << 0.9 - name_width - val_width << "\\linewidth}|}\\nobreakhline" << endl;
    stream << QString("\\multicolumn{3}{|p{0.95\\linewidth}|}{\\textbf{Тип:} \\texttt{") << endl;
    stream << f_save_plain_text(struct_descr->name());

    stream << "}}\\\\*\\nobreakhline" << endl;

    stream << QString("\\multicolumn{3}{|p{0.95\\linewidth}|}{\\textbf{Описание:}") << endl;
    stream << f_save_text(struct_descr->descrElem());
    stream << "}\\\\*\\nobreakhline" << endl;

    if (struct_descr->fieldsCount()) {
        stream << QString("\\textbf{Поле} & \\textbf{Тип} & \\textbf{Описание поля}\\\\*\\nobreakhline");
        for (int i=0; i < struct_descr->fieldsCount(); i++) {
            if (!struct_descr->field(i)->name().startsWith('@'))
                f_save_field_descr(stream, struct_descr->field(i), i != 0);
        }
    }


    stream << "\\end{longtable}" << endl;
    stream << f_table_section_end();
    return 0;
}


static int f_convert_tex(QTextStream &dstream, QTextStream &sstream) {
    while (!sstream.atEnd()) {
        QString line = sstream.readLine();

        /* удаляем все разрешенные doxygen переносы, так как переносы в названиях
         * функций нам не очень желательны */
        /** @todo сделать систему расстановки переносов для слишком длинных названий,
         *  например в местах символов _ */
        line.remove("\\-");
        /* doxygen mbox использует в ссылках, но это мешает переносу длинных ссвлок,
         * что может быть очень проблематично для форматирования документа. */
        line.remove("\\mbox");

        /* просматриваем все ссылки. Если ссылка ссылается на секцию,
         * заменяем ее на hyperref, так как он работает лучше в плане
         * точного указания на начало секции, чем \hyperlink */
        int cur_ind = 0, ind = 0, ind2 = 0;
        do {
            QString hyp_start = "\\hyperlink{";
            ind = line.indexOf(hyp_start, cur_ind);
            if (ind >= 0) {
                ind2 = line.indexOf('}', ind);
                if (ind2 >= 0) {
                    QString id = line.mid(ind + hyp_start.length(), ind2-(ind+hyp_start.length()));
                    bool sec_ref = false;
                    Q_FOREACH(QString sec_id, sect_id_list) {
                        if (sec_id.trimmed()==id.trimmed()) {
                            sec_ref = true;
                            break;
                        }
                    }
                    if (sec_ref) {
                        line.replace(ind, hyp_start.length(), "\\hyperref[");
                        ind2 = line.indexOf('}', ind);
                        line.replace(ind2, 1, ']');
                    }
                    cur_ind = ind2;
                } else {
                    ind = ind2;
                }
            }
        } while (ind>=0);

        f_correct_quote(line, "\\char`\\\"{}");
        f_correct_quote(line, "\"");
        dstream << line << endl;

    }
    return 0;
}



typedef  enum {
    FILE_PROC_TYPE_XML = 0,
    FILE_PROC_TYPE_TEX = 1
} t_file_proc_type;

class t_file_descr {
public:
    QString srcFile() const {return m_srcFile;}
    QString destFile() const {return m_destFile;}
    int sectLvl() const {return m_lvl;}
    t_file_proc_type type() const {return m_type;}
    bool sortByTypes() const {return m_sort_by_types;}


    t_file_descr(QDomElement elem);

    t_file_descr(QString xml_file, QString tex_file, int level, t_file_proc_type type = FILE_PROC_TYPE_XML) :
        m_srcFile(xml_file), m_destFile(tex_file), m_lvl(level), m_type(type) {}
private:

    QString m_srcFile;
    QString m_destFile;
    int m_lvl;
    t_file_proc_type m_type;
    bool m_sort_by_types;
} ;

t_file_descr::t_file_descr(QDomElement elem) {
    m_srcFile = elem.text().trimmed();
    m_destFile = elem.attribute("output");
    m_lvl = elem.attribute("sect_lvl", "0").toInt();
    QString type_str = elem.attribute("type", "xml");
    if (type_str=="xml") {
        m_type = FILE_PROC_TYPE_XML;
    } else if (type_str=="tex") {
        m_type = FILE_PROC_TYPE_TEX;
    }

    m_sort_by_types = elem.attribute("sort_by_types", "no").trimmed() == "yes" ? true : false;
}





static int f_process_tex_file(t_file_descr& fdescr) {
    QFile src_file(fdescr.srcFile());
    if (!src_file.open(QIODevice::ReadOnly))
         return -4;
    QFile dst_file(fdescr.destFile());
    if (!dst_file.open(QIODevice::WriteOnly)) {
        src_file.close();
        return -5;
    }

    QTextStream sstream(&src_file);
    QTextStream dstream(&dst_file);
    sstream.setCodec("UTF-8");
    dstream.setCodec("UTF-8");


    f_convert_tex(dstream, sstream);

    src_file.close();
    dst_file.close();
    return 0;
}


static int f_process_file(t_file_descr& fdescr) {
    QList<doxyElemDescr*> clist;
    int err = parseDoxyFile(fdescr.srcFile(), clist);
    if (!err && !clist.size())
        err = -6;
    if (!err){
        QFile tex(fdescr.destFile());

        if (!tex.open(QIODevice::WriteOnly)) {
            err = -6;
        } else {
            QTextStream stream(&tex);
            stream.setCodec("UTF-8");

            for (int i = 0; i < clist.size(); i++) {
                doxyCompoundDescr* comp = dynamic_cast<doxyCompoundDescr*>(clist.at(i));
                doxyElemType* structElem = dynamic_cast<doxyElemType*>(clist.at(i));
                if (comp) {
                    QString name = comp->kind()=="group" ? comp->title() : comp->name();
                    stream << f_save_section(name, comp->id(), fdescr.sectLvl());

                    int member_lvl = fdescr.sortByTypes() ? fdescr.sectLvl()+2 : fdescr.sectLvl() + 1;

                    stream << f_save_text(comp->descrElem());

                    if (fdescr.sortByTypes()) {
                        QList<doxyCompoundDescr*> files = comp->fileList();
                        if (files.size()) {
                            stream << f_save_section(QObject::tr("Файлы."),
                                                 comp->id()+"_files", fdescr.sectLvl()+1);
                            stream << "\\begin{tabular}{|m{0.35\\linewidth}|m{0.55\\linewidth}|}\\hline" << endl;
                            //stream << "\\rowcolor{gray!10}";
                            stream << QObject::tr("Файл") << "&" << QObject::tr("Описание") <<
                                      "\\\\\\hline" << endl;

                            Q_FOREACH(doxyCompoundDescr *file, files) {
                                //stream << "\\rowcolor{lightgray}";
                                stream << f_save_ref_text(file->name(), file->id()) << "&";
                                stream << f_save_text(file->descrElem());
                                stream << "\\\\\\hline" << endl;
                            }

                            stream << "\\end{tabular}" << endl;
                        }
                    }


                    QList<doxyElemField*> variables = comp->variablesList();
                    if (!variables.isEmpty()) {
                        stream << f_save_section(QObject::tr("Глобавльные переменные"),
                                                 comp->id()+"_variables", fdescr.sectLvl()+1);

                        stream << "\\begin{tabular}{|m{0.22\\linewidth}|m{0.23\\linewidth}|m{0.43\\linewidth}|}\\hline" << endl;
                        stream << QString("\\textbf{Переменная} & \\textbf{Тип} & \\textbf{Описание}\\\\\\hline");
                        for (int i=0; i < variables.size(); i++) {
                            f_save_field_descr(stream, variables.at(i), true);
                        }
                        stream << "\\end{tabular}";
                    }


                    QList<doxyElemEnumVal> defines = comp->definesList();
                    if (defines.size()) {
                        stream << f_table_section_start();

                        stream << f_save_section(QObject::tr("Константы и макроопределения"),
                                                     comp->id()+"_defines", fdescr.sectLvl()+1);                        
                        f_save_defines(stream, defines, member_lvl);
                        stream << f_table_section_end();
                    }

                    QList<doxyElemEnum> enums = comp->enumList();
                    if (enums.size()) {
                        if (fdescr.sortByTypes()) {
                            stream << f_save_section(QObject::tr("Перечисления"),
                                                     comp->id()+"_enums", fdescr.sectLvl()+1);                            
                        }

                        Q_FOREACH(doxyElemEnum enumElem, enums) {
                            f_save_enum_descr(stream, enumElem, member_lvl);
                        }
                    }

                    QList<doxyElemType*> structs = comp->structList();
                    if (structs.size()) {
                        if (fdescr.sortByTypes()) {
                            stream << f_save_section(QObject::tr("Структуры"),
                                                     comp->id()+"_structs", fdescr.sectLvl()+1);
                        }

                        Q_FOREACH(doxyElemType* structElem, structs) {
                            if (structElem->paramsCnt()) {
                                f_save_func_type(stream, *structElem, member_lvl);
                            } else {
                                f_save_struct(stream, structElem, member_lvl);
                            }
                        }
                    }

                    QList<doxyElemFunc> funcs = comp->funcList();
                    if (funcs.size()) {
                        if (fdescr.sortByTypes()) {
                            stream << f_save_section(QObject::tr("Функции"),
                                                     comp->id()+"_func", fdescr.sectLvl()+1);
                        }

                        Q_FOREACH(doxyElemFunc funcElem, funcs) {
                            f_save_func_descr(stream, funcElem, member_lvl);
                        }
                    }
                } else if (structElem) {
                    f_save_struct(stream, structElem, fdescr.sectLvl());
                }
            }

            tex.close();
        }
    }

    qDeleteAll(clist);
    return err;
}



int main(int argc, char *argv[]) {
#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
    QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));
    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));
#endif

    QList<t_file_descr> fileList;

    if (argc < 2)
        return -1;

    QDomDocument doc;
    QFile file(argv[1]);
    if (!file.open(QIODevice::ReadOnly))
         return -2;

    if (!doc.setContent(&file)) {
         file.close();
         return -3;
    }
    file.close();

    if (doc.documentElement().tagName()!="doxyXmlToLatexConfig") {
         return -4;
    }

     for (QDomElement elem = doc.documentElement().firstChildElement();
          !elem.isNull(); elem = elem.nextSiblingElement()) {
         if (elem.tagName() == "file") {
             fileList << t_file_descr(elem);
         }
     }

     /* первый раз проходим все xml-файлы только для получения статической
      * информации для всех секций */
     Q_FOREACH(t_file_descr filedescr, fileList) {
         if (filedescr.type()==FILE_PROC_TYPE_XML)
             f_process_file(filedescr);
     }

     /* второй проход уже окончательный */
     Q_FOREACH(t_file_descr filedescr, fileList) {
         switch (filedescr.type()) {
             case FILE_PROC_TYPE_XML:
                 f_process_file(filedescr);
                 break;
             case FILE_PROC_TYPE_TEX:
                 f_process_tex_file(filedescr);
                 break;
         }
     }
    return 0;

}


