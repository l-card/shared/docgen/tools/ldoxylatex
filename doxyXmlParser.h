#ifndef DOXYXMLPARSER_H
#define DOXYXMLPARSER_H

#include <QString>
#include <QList>
#include <QDomElement>



typedef enum
{
    FUNC_PARAM_DIR_IN,
    FUNC_PARAM_DIR_OUT,
    FUNC_PARAM_DIR_INOUT
} t_func_par_dir;



/** стандартное описание элемента doxygen */
class doxyElemDescr
{
public:
    QString id() const {return m_id;} /**< id дляссылки */
    QString name() const {return m_name;} /**< названиеэлемента */
    QString briefDescr() const {return m_briefDescr;} /**< краткое описание */
    QDomElement descrElem() const {return m_descr;} /**< полный пример */

    virtual ~doxyElemDescr() {}

protected:
    /** разбор параметров элемента по главному XML-элементов */
    virtual void parseFromElemnt(QDomElement elem);
    /** разбор параметра по дочернему XML-элементу */
    virtual void parseChild(QDomElement elem);

    QString m_name;
    QDomElement m_descr;
    QString m_id;
    QString m_briefDescr;

};


class doxyElemEnumVal : public doxyElemDescr
{
public:
    doxyElemEnumVal(QDomElement elem) : doxyElemDescr() {parseFromElemnt(elem);}
    QString initializer() const {return m_initializer;}
protected:
    virtual void parseChild(QDomElement elem);
private:
    QString m_initializer;
};

class doxyElemEnum : public doxyElemDescr
{
public:
    doxyElemEnum(QDomElement elem) : doxyElemDescr() {parseFromElemnt(elem);}

    int valsCount() const {return m_vals.size();}
    const doxyElemEnumVal& val(int i) const {return m_vals.at(i);}
    const QList<doxyElemEnumVal> valList() const {return m_vals;}
protected:
    virtual void parseChild(QDomElement elem);
private:
    QList<doxyElemEnumVal> m_vals;
};


class doxyElemFuncPar : public doxyElemDescr
{
public:
    doxyElemFuncPar(QDomElement elem) : doxyElemDescr() {parseFromElemnt(elem);}
    t_func_par_dir dir() {return m_dir;}
protected:
    virtual void parseChild(QDomElement elem);
private:
    t_func_par_dir m_dir;
};

/** описание функции */
class doxyElemFunc : public doxyElemDescr
{
public:
    virtual QString definition() const {return m_def + " " + m_argstr;}
    QDomElement returnElem() const {return m_retElem;}
    int paramsCnt() const {return m_params.size();}
    doxyElemFuncPar param(int i) const {return m_params.at(i);}

    doxyElemFunc(QDomElement elem) : doxyElemDescr() {parseFromElemnt(elem);}
protected:
    virtual void parseFromElemnt(QDomElement elem);
    virtual void parseChild(QDomElement elem);
private:
    QString m_def; /**< определение функции */
    QString m_argstr;
    QList<doxyElemFuncPar> m_params; /**< список описаний параметров */
    QDomElement m_retElem;
};

class doxyElemField : public doxyElemDescr
{
public:
    doxyElemField(QDomElement elem) : doxyElemDescr() {parseFromElemnt(elem);}
    QDomElement type() const {return m_type;}
    QDomElement typeModifier() const {return m_typeModifier;}

protected:
    virtual void parseChild(QDomElement elem);
private:
    QDomElement m_type;
    QDomElement m_typeModifier;
};


/** описание структуры */
class doxyElemType : public doxyElemFunc
{
public:
    doxyElemType(QDomElement elem) : doxyElemFunc(elem)
    {
        parseFromElemnt(elem);
    }
    int fieldsCount() const {return m_fields.length();}
    const doxyElemField* field(int i) const {return &m_fields.at(i);}

protected:
    virtual void parseChild(QDomElement elem);
private:
    QList<doxyElemField> m_fields;
};


class doxyCompoundDescr : public doxyElemDescr
{
public:
    doxyCompoundDescr(QDomElement elem, QString filename) : doxyElemDescr() {m_filename = filename; parseFromElemnt(elem);}
    virtual ~doxyCompoundDescr() {qDeleteAll(m_fileList); qDeleteAll(m_structList);}

    QString title() const {return m_title;}
    QString kind() const {return m_kind;}

    QList<doxyElemFunc> funcList() const {return m_funcList;}
    QList<doxyElemEnum> enumList() const {return m_enumList;}
    QList<doxyElemType*> structList() const {return m_structList; }
    QList<doxyElemEnumVal> definesList() const {return m_definesList; }
    QList<doxyCompoundDescr*> fileList() const {return m_fileList;}
    QList<doxyElemField*> variablesList() const {return m_variablesList;}
protected:
    virtual void parseFromElemnt(QDomElement elem);
    virtual void parseChild(QDomElement elem);
private:
    QString m_title;
    QString m_kind;
    QString m_filename;
    QList<doxyElemFunc> m_funcList;
    QList<doxyElemEnum> m_enumList;
    QList<doxyElemType*> m_structList;
    QList<doxyElemEnumVal> m_definesList;
    QList<doxyCompoundDescr*> m_fileList;
    QList<doxyElemField*> m_variablesList;
};



int parseDoxyFile(QString filename, QList<doxyElemDescr*>& list);


#endif // DOXYXMLPARSER_H





